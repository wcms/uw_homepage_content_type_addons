<?php

/**
 * @file
 * homepage_content_type_addons.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function homepage_content_type_addons_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
