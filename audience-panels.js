/**
 * @file
 */

(function ($) {

$(function () {

  var $tabsContainer = $('#quicktabs-homepage_audience_panel_items ul.quicktabs-tabs')
  if ($tabsContainer.length) {
    var $tabs = $('a', $tabsContainer).blur(); // Ie issue where it focuses on a different tab on a page refresh, call blur() to clear it.
    var numTabs = $tabs.length;

    // Open the last active audience tab based on cookie settings (set in quicktabs.override.js)
    if ($.cookie('uw-audience-tab')) {
      openTab = $.cookie('uw-audience-tab');
      // alert(openTab);
      $('#' + openTab).trigger('click');
    }

    // Keyboard help text.
    var $keyboardHelp = $('<div>&larr; or &rarr; to change panels. TAB to select first panel item.</div>').addClass('element-invisible keyboard-help');
    $keyboardHelp.insertBefore($tabsContainer);

    $tabs.keydown(function (event) {

      var selected = $('a', $tabsContainer);

      // Key codes
      // TAB    9
      // LEFT   37
      // UP     38
      // RIGHT  39
      // DOWN   40.
      // RIGHT or DOWN key, open next tab.
      if (event.which == 39 || event.which == 40) {
        event.preventDefault();
        $nextslide = $('li.active', $tabsContainer).next().find('a');
        // If there is not a next slide, go to the first slide.
        if (!$nextslide.length) {
          $nextslide = $('li:first a', $tabsContainer);
        }
        $nextslide.click().focus();
        $keyboardHelp.removeClass('element-invisible');
     }
      // LEFT or UP key, open previous tab.
      else if (event.which == 37 || event.which == 38) {
        event.preventDefault();
        $nextslide = $('li.active', $tabsContainer).prev().find('a');
        // If there is not a next slide, go to the first slide.
        if (!$nextslide.length) {
          $nextslide = $('li:last a', $tabsContainer);
        }
        $nextslide.click().focus();
        $keyboardHelp.removeClass('element-invisible');
      }
      else if (event.shiftKey) {
        // Do nothing.
      }
      // TAB, focus on first link in panel.
      else if (event.which == 9) {
        event.preventDefault();
        $('a:first', '#quicktabs-container-homepage_audience_panel_items .quicktabs-tabpage:not(.quicktabs-hide)').focus();
        $keyboardHelp.addClass('element-invisible');
      }

    });

    $tabs.focus(function (event) {
      $keyboardHelp.removeClass('element-invisible');
    });

    $tabs.blur(function (event) {
      $keyboardHelp.addClass('element-invisible');
    });
  }

});

})(jQuery);
