/**
 * @file
 */

(function ($) {
  /*
   * this function overrides the Quicktabs module's default click handler to provide transitions for certain quicktabs,
   * while preserving the default behaviour for the rest. (If Quicktabs' click handler changes, this code will have to as well.)
   */
  if (typeof Drupal.quicktabs != 'undefined') {
    Drupal.quicktabs.clickHandlerAddons = function (event) {
      var tab = event.data.tab;
      var element = this;

      // Determine whether or not to save the active tab selected as a cookie (audience panel items)
      if (event.data.tab.container.selector == '#quicktabs-container-homepage_audience_panel_items') {
        $.cookie('uw-audience-tab', event.data.tab.element.id, {expires: 30});
      }

      // Determine whether or not to animate the transitions (only for certain quicktab IDs)
      animateTransition = $.inArray(tab.container.attr('id'),['quicktabs-container-homepage_feature_stories','quicktabs-container-homepage_feature_stories_future','quicktabs-container-homepage_feature_stories_current','quicktabs-container-homepage_feature_stories_faculty','quicktabs-container-homepage_feature_stories_staff']);
      // Find the currently active tab.
      oldTab = tab.container.children().not('.quicktabs-hide');
      // Set clicked tab to active.
      $(this).parents('li').siblings().removeClass('active');
      $(this).parents('li').addClass('active');

      // Hide all tabpages.
      tab.container.children().addClass('quicktabs-hide');

      if (!tab.tabpage.hasClass("quicktabs-tabpage")) {
        tab = new Drupal.quicktabs.tab(element);
      }
      tab.tabpage.removeClass('quicktabs-hide');
      if (animateTransition > -1) {
        if (oldTab.attr('id') != tab.tabpage.attr('id')) {
          fadespeed = 400;
          oldTab.fadeIn(0).fadeOut(fadespeed);
          tab.tabpage.fadeOut(0).fadeIn(fadespeed);
        }

        // If we are animating, prepare the next cycle.
        if (fsautoclick) {
          fsautoclick = false;
          fs_start();
        }
        else {
          // We were not automating, so stop any animation.
          fs_stop();
        }
      }

      // Find any classes that start with org (the colour class), remove it.
      $('div[id*="quicktabs-homepage_feature_stories"] .item-list').attr('class',
        function (i, c) {
          return c.replace(/(^|\s)org\S+/g, '');
      });

      // Get the class list for the div containing the play/pause button.
      var classList = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide) .banner-item').attr("class").split(' ');

      // If there is at least one class, add the faculty class or remove it.
      if (classList.length > 1) {

        // If there is a faculty, add that class to the div with the play/pause button.
        if (classList[3] !== undefined) {
          $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass(classList[3]);
        }
        else {
          $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass('org_default_bg');
        }
      }

      return false;
    }

    fs_nextslide = function () {
      slidedivs = fsslidedivs;
      nextslide = $('li.active', slidedivs).next().find('a');
      // If there is not a next slide, go to the first slide.
      if (!nextslide.length) {
        nextslide = $('li:first a', slidedivs);
      }
      fsautoclick = true;
      Drupal.behaviors.addBannerControls.nextSlide();
      fs_start();
    }

    fs_start = function (delay) {
      if (delay === undefined) {
        delay = 7000;
      }
      fsanimate = setTimeout(fs_nextslide,delay);
      fsbutton.removeClass('play').addClass('pause').attr('title','Pause').find('.element-invisible').html('Pause slideshow');
    }

    fs_stop = function () {
      clearTimeout(fsanimate);
      fsbutton.removeClass('pause').addClass('play').attr('title','Play').find('.element-invisible').html('Play slideshow');
    }

    $(function () {
      // Slide container.
      fsslidedivs = '#featured .quicktabs-wrapper';

      // If there is a list item that has both the .first and .last classes, hide it (there is only 1 featured item)
      // else we add a play/pause button and start the slideshow.
      if ($('li.first.last', fsslidedivs).length) {
        $('.item-list', fsslidedivs).addClass('element-hidden');
      }
      else {
        // Add the play/pause button.
        fsbutton = $('<button class="slideshow-control pause" title="Pause"><span class="element-invisible">Pause slideshow</span></button>');
        $('.item-list', fsslidedivs).prepend(fsbutton);

        // Start the slideshow.
        fs_start();

        // Handle play/pause clicks.
        fsbutton.click(function () {
          if ($(this).hasClass('play')) {
            fs_start(0);
          }
          else {
            fs_stop();
          }
        });
      }

    });
  }
})(jQuery);
